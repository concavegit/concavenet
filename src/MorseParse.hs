module MorseParse where

import           Data.Void
import           System.Hardware.WiringPi
import           Text.Megaparsec

type Parser = Parsec Void [Value]
