module Morse
  ( Symbol (..)
  , Time (..)
  , encode
  , decode
  , send
  ) where

import           Control.Arrow
import           Control.Concurrent
import           Control.Monad.Reader
import           Data.Char
import           Data.Foldable
import           Data.List
import           Data.List.Split
import           Data.Map                 (Map)
import qualified Data.Map                 as Map
import           Data.Tuple
import           System.Hardware.WiringPi

data Symbol = Dot | Dash | SSpc | LSpc | WSpc deriving Show
data Time = Short | Medium | Long deriving Show
type PinOp = ReaderT Pin IO

spaceUnit :: Int
spaceUnit = 1000000

hold :: Int -> PinOp ()
hold = ReaderT . const . flip replicateM_ (threadDelay spaceUnit)

setMode :: Value -> PinOp ()
setMode = ReaderT . flip digitalWrite

morseMap :: Map Char [Symbol]
morseMap = Map.fromList $ alpha ++ spec
  where alpha  = second (intersperse SSpc)
          <$>
          [ ('a', [Dot, Dash])
          , ('b', [Dash, Dot, Dot, Dot])
          , ('c', [Dash, Dot, Dash, Dot])
          , ('d', [Dash, Dot, Dot])
          , ('e', [Dot])
          , ('f', [Dot, Dot, Dash, Dot])
          , ('g', [Dash, Dash, Dot])
          , ('h', [Dot, Dot, Dot, Dot])
          , ('i', [Dot, Dot])
          , ('j', [Dot, Dash, Dash, Dash, Dash])
          , ('k', [Dash, Dot, Dash])
          , ('l', [Dot, Dash, Dot, Dot])
          , ('m', [Dash, Dash])
          , ('n', [Dash, Dot])
          , ('o', [Dash, Dash, Dash, Dash])
          , ('p', [Dot, Dash, Dash, Dot])
          , ('q', [Dash, Dash, Dot, Dash])
          , ('r', [Dot, Dash, Dot])
          , ('s', [Dot, Dot, Dot])
          , ('t', [Dash])
          , ('u', [Dot, Dot, Dash])
          , ('v', [Dot, Dot, Dot, Dash])
          , ('w', [Dot, Dash, Dash])
          , ('x', [Dash, Dot, Dot, Dash])
          , ('y', [Dash, Dot, Dash, Dash])
          , ('z', [Dash, Dash, Dot, Dot])
          , ('1', Dot:replicate 4 Dash)
          , ('2', [Dot, Dot, Dash, Dash, Dash])
          , ('3', [Dot, Dot, Dot, Dash, Dash])
          , ('4', replicate 4 Dot ++ [Dash])
          , ('5', replicate 5 Dot)
          , ('6', Dash:replicate 4 Dot)
          , ('7', [Dash, Dash, Dot, Dot, Dot])
          , ('8', [Dash, Dash, Dash, Dot, Dot])
          , ('9', replicate 4 Dash ++ [Dot])
          , ('0', replicate 5 Dash)
          , ('.', [Dot, Dash, Dot, Dash, Dot, Dash])
          , (',', [Dash, Dash, Dot, Dot, Dash , Dash])
          , ('?', [Dot, Dot, Dash, Dash, Dot, Dot])
          , ('\'', Dot:replicate 4 Dash ++ [Dot])
          , ('!', [Dash, Dot, Dash, Dot, Dash, Dash])
          ]

        spec = [(' ', [WSpc])]

valsMorseMap :: Map [Value] Char
valsMorseMap = Map.fromList . map swap . Map.toList
  $ Map.map (>>= symbToVals) morseMap

symbToVals :: Symbol -> [Value]
symbToVals Dot  = [HIGH]
symbToVals Dash = [HIGH, HIGH, HIGH]
symbToVals SSpc = [LOW]
symbToVals LSpc = [LOW, LOW, LOW]
symbToVals WSpc = replicate 7 LOW

strToSymb :: String -> Maybe [Symbol]
strToSymb = fmap (intercalate [WSpc])
  . traverse (fmap (intercalate [LSpc]) . traverse (morseMap Map.!?))
  . words . map toLower

symbsToVals :: [Symbol] -> [Value]
symbsToVals = (>>= symbToVals)

encode :: String -> Maybe [Value]
encode = fmap symbsToVals . strToSymb

send :: [Value] -> PinOp ()
send = traverse_ ((*> hold spaceUnit) . setMode)

decode :: [Value] -> Maybe String
decode = fmap unwords
  . traverse (traverse (valsMorseMap Map.!?) . splitOn [LOW, LOW, LOW])
  . splitOn (replicate 7 LOW)
